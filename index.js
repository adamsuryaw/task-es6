//No 1
const yonko = ["Kaido", "Big Mom", "Shanks", "Kurohige", "Luffy"];

//No 2
const mugiwara = {
    kapten : "Luffy",
    wakil : "Roronoa Zoro",
    chef : "Sanji",
    navigator : "Nami",
    shooter : "God Usop",
    dokter : "Tony Tony Chopper",
    arkeolog : "Nico Robin",
    carpenter : "Franky",
    pemusik : "Soul King",
    driver : "Jinbei",
    kapal : {
        kapal1 : "Going Merry",
        kapal2 : "Sunny Go"
    }
}

//No 3
const {kapten, wakil, chef, navigator, shooter, dokter, arkeolog, carpenter, pemusik, driver, kapal} = mugiwara;
console.log(kapten, kapal);
const [one, two, three, four, five] = yonko;
console.log(one, two);

//No 4
console.log(`${yonko[4]} telah menjadi yokno menggunakan kapal ${mugiwara.kapal.kapal2}`)


//No 5
const shicibukai = (1 < 7) ? "Bubar" : "Lanjut"
console.log(shicibukai)

//No 6
let ship = [
    {captain: "Roger", name: "Oro Jacksons", power: 200},
    {captain: "Whitebird", name: "Moby Dick", power: 180},
    {captain: "Bege", name: "Nostra Castello", power: 150},
    {captain: "Law", name: "Polar Tang", power: 150},
    {captain: "Moria", name: "Thriller Bark", power: 500}
]

//No 7
// const bigShip = ship.map(ship => ship.name);
// console.log(bigShip)

ship.map((nama) => {
    console.log(`Kapten ${nama.captain} adalah pemimpin kapal ${nama.name}`)
})

//No 8
const strength = ship.filter(kuat => kuat.power > 180)
console.log(strength)

//No 9
const kekuatan = ship.find(kuat => kuat.power > 180)
console.log(kekuatan)